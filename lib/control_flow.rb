# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.each_char do |char|
    if char.downcase == char
      str.delete!(char)
    end
  end
  str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  length=str.length
  if length % 2 ==0
    newstring = str[length/2 -1] + str [length/2]
  else
    newstring = str[length/2]
  end
  newstring
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  counter = 0
  str.each_char do |char|
    if VOWELS.include?(char)
      counter+=1
    end
  end
  counter
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).reduce(:*)

end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  newstring=''
  arr.each_with_index do |ele,idx|
    if idx + 1 == arr.length
      return newstring += ele.to_s
    end
    newstring+= (ele.to_s + separator)
  end
  newstring
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  newstring =''
  str.chars.each_with_index do |char, idx |
    if (idx-1) % 2 == 0
      newstring += char.upcase
    else
      newstring += char.downcase
    end
  end
  newstring
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  newstring=[]
  str.split(" ").each do |word|
    if word.length > 4
      newstring.push(word.reverse)
    else
      newstring.push(word)
    end
  end
  newstring.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  newarray=[]
  (1..n).each do |num|
    string=''
    if num % 3 == 0
      string += "fizz"
    end
    if num % 5 == 0
      string += "buzz"
    end
    if string.length > 0
      newarray.push(string)
    else
      newarray.push(num)
    end
  end
  newarray
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order. could have used a counter and indexed into the array at length of array
# minus counter adding that value to a new array and after ending the counter loop returning the new array.
# this was easier.
def my_reverse(arr)
  arr=arr.reverse
end

# Write a method that returns a boolean indicating whether the argument is
# prime. functions for positive numbers.
def prime?(num)
  if num == 1
    return false
  end
  (2..(num-1)).each do |value|
    if num % value == 0
      return false
    end
  end
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  factors=[]
  (1..num).each do |value|
    if num % value == 0
      factors.push(value)
    end
  end
  factors
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors_prime =[]
  (1..num).each do |value|
    if num % value == 0 && factors(value).length == 2
      factors_prime.push(value)
    end
  end
  factors_prime
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odd=[]
  even=[]
  arr.each do |value|
    if value % 2 ==0
      even.push(value)
    else
      odd.push(value)
    end
  end
  if even.length == 1
    return even[0]
  end
  return odd[0]
end
